# [asm4pg](https://forgemia.inra.fr/asm4pg/GenomAsm4pg)

Asm4pg is an automatic and reproducible genome assembly workflow for pangenomic applications using PacBio HiFi data.

Documentation: [GitLab Pages](https://asm4pg.pages.mia.inra.fr/genomasm4pg)

## All Options

Asm4pg has many options with default values. To modify them, refer to the [Going Further](doc/going_further.md) section.

## Outputs

To learn more about the workflow's output files, refer to the [Outputs](doc/outputs.md) section.

## Known Errors

You may encounter [these errors](doc/known_errors.md).

## Software

Here is a list of [software used in the workflow](doc/software_list.md).
